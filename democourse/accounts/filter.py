from django.db.models import fields
import django_filters
from django_filters import DateFilter
from .models import *


class OrderFilter(django_filters.FilterSet):
    """
    Description: Model Description
    """
    start_date = DateFilter(field_name='data_created', lookup_expr='gte')
    start_date = DateFilter(field_name='data_created', lookup_expr='gte')
    class Meta:
    	model = Order
    	fields = '__all__'
    	exclude = ['customer','data_created']
        