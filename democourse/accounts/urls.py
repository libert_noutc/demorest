from os import name
from django.urls import path, include
from .serialiser import *
from rest_framework.routers import DefaultRouter
from .views import *

# Routers provide an easy way of automatically determining the URL conf.
router = DefaultRouter()

router.register('users', UserViewSet, basename='user')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.


urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('products', ProductView.as_view(), name='products'),
    path('customer/<str:pk>', ListCustomers.as_view(), name='customer'),
    path('login/', LoginView.as_view(), name='login'),
    path('user/', UserView.as_view(), name='user'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('register/' , RegisterView.as_view(), name='register'),
    path('create_order/<str:pk>', CustomersView.as_view(), name='create_order'),
    path('update_order/<str:pk>', UpdateCustumer.as_view(), name='update_order'),
    path('delete_order/<str:pk>', DeleteCustumer.as_view(), name='delete_order'),


]
