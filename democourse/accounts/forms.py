from .models import Custumer
from django.db.models import fields
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm


class OrderForm(ModelForm):
    """
    Description: Model Description
    """
    

    class Meta:

        model = Custumer
        fields = '__all__'


class CreateUserForm(UserCreationForm):
    """
    Description: Model Description
    """
    

    class Meta:
        model = User
        fields = ['username', 'email','password1', 'password2']
        pass