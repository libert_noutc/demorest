from django.db.models.signals import post_save
from django.contrib.auth.models import UserManager
from .models import Custumer


def Custumer_profile(sender, instance, create, *kwargs):
    if create:
        group = Group.objects.get(name='customer')
        instance.groups.add(group)

        Custumer.objects.create(
            user=instance,
            name=instance.username,)

post_save.connect(Custumer_profile, sender=User)