from os import name
from .decorators import *
from django import views
from django.contrib.messages.api import success
from django.shortcuts import redirect, render
from django.utils import module_loading
from django.views import View
from django.contrib.auth.decorators import login_required
from django.forms import inlineformset_factory
from .models import *
from .forms import *
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import Group
# Create your views here.


decorators = [allowed_users(allowed_roles=['admin']), admin_only]


@method_decorator(decorators, name='get')
class DeleteCustumer(View):
    """
    Description: Model Description
    """
    template_name = 'accounts/delete.html'

    def get(self, request, *args, **kwargs):
        oders = Order.objects.all()
        customer = Custumer.objects.all()

        context = {
            'oders': oders,
            'customers': customer,

        }
        return render(request, self.template_name, context)

    def post(self, pk, request, *args, **kwargs):
        OrderFormset = inlineformset_factory(
            Custumer, Order, fields=('products', 'status'))
        customer = Custumer.objects.get(id=pk)
        # form = OrderForm(initial={ 'customers': customer})
        form = OrderFormSet(instance=customer)

        if form.is_valid():
            form.save()
            return redirect('/')
            pass
        context = {

            'formset': customer,

        }
        return render(request, self.template_name, context)


decorators = [allowed_users(allowed_roles=['admin']), admin_only]


@method_decorator(decorators, name='get')
class CustomersView(View):
    """
    Description: Model Description
    """
    template_name = 'accounts/order_form.html'

    def get(self, pk, request, *args, **kwargs):
        oders = Order.objects.all()
        customer = Custumer.objects.get(id=pk)

        # OrderFormset = inlineformset_factory(Custumer, Order, fields=('products', 'status'))
        orders = customer.order_set.all()
        orders_account = orders.count()

        context = {

            'customer': customer,
            'orders': orders,
            'order_account': orders_account,

        }
        return render(request, 'accounts/customer.html', context)

    def post(self, pk, request, *args, **kwargs):

        customer = Custumer.objects.get(id=pk)
        # OrderFormset = inlineformset_factory(Custumer, Order, fields=('products', 'status'))
        # orders = customer.order_set.all()
        # orders_account = orders.count()
        # form = OrderForm(initial={ 'customers': customer})

        form = OrderForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('/')

        context = {

            'form': form
        }
        return render(request, self.template_name, context)


decorators = [allowed_users(allowed_roles=['admin']), admin_only]


@method_decorator(decorators, name='get')
class UpdateCustumer(View):
    template_name = 'accounts/customer.html'

    def get(self):
        pass


class ListCustomers(View):
    template_name = 'accounts/customer.html'

    def get(self, request, pk, *args, **kwargs):
        customer = Custumer.objects.get(id=pk)
        orders = customer.order_set.all()
        order_account = orders.count()
        context = {
            'customers': customer,
            'orders': orders,
            'count_orders': order_account,
        }
        return render(request, self.template_name, context)

        pass


decorators = [allowed_users(allowed_roles=['admin']), admin_only]


@method_decorator(decorators, name='get')
class HomeView(View):
    """
    Description: Model Description
    """
    template_name = 'accounts/dashboard.html'

    def get(self, request, *args, **kwargs):

        oders = Order.objects.all()
        customer = Custumer.objects.all()
        total_orders = oders.count()
        total_customers = customer.count()
        delivered = oders.filter(status='Delivred').count()
        pending = oders.filter(status='Pending').count()

        context = {
            'oders': oders,
            'customers': customer,
            'total_orders': total_orders,
            'total_customers': total_customers,
            'delivered': delivered,
            'pending': pending,

        }
        return render(request, self.template_name, context)


class ProductView(View):
    """
    Description: Model Description
    """
    template_name = 'accounts/products.html'

    def get(self, request, *args, **kwargs):
        products = Product.objects.all()
        context = {

            'products': products,
        }
        return render(request, self.template_name, context)



class UserView(View):
    """
    Description: Model Description
    """
    template_name = 'accounts/user.html'

    def get(self, request, *args, **kwargs):
      
        context = {
            'form': ''
        }
        return render(request, self.template_name, context)


   


class LoginView(View):
    """
    Description: Model Description
    """
    template_name = 'accounts/login.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('home')

        context = {
            'form': ''
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            messages.info(request, 'username or password is incorect ')

        context = {
            'form': 'form'
        }
        return render(request, self.template_name, context)


class LogoutView(View):
    """
    Description: Model Description
    """

    def get(self, request):
        logout(request)

        return redirect('login')


class RegisterView(View):
    """
    Description: Model Description
    """
    template_name = 'accounts/register.html'

    def get(self, request, *args, **kwargs):

        form = CreateUserForm()
        context = {'form': form}

        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):

        form = CreateUserForm(request.POST)

        if form.is_valid():
            user = form.cleaned_data['username']
            form.save()
            
            messages.success(request, 'accounts was created for ' + user)
            return redirect('login')

        context = {'form': form}
        return render(request, self.template_name, context)
