from django import views
from django.http import HttpResponse
from django.shortcuts import redirect


def unauthenticated_user(view):

    def wrapper_func(request, *args, **kwargs):
        if request.user.is_authenticated:    	
            return redirect('/')
        
        return view(request, *args, **kwargs)

    return wrapper_func


def allowed_users(allowed_roles=[]):
    def decorator(view):
        def wrapper_func(request, *args, **kwargs):
            group = None
            if request.user.groups.exists():
                group = request.user.groups.all()[0].name
            print(group , '== ', allowed_roles)
            if group in allowed_roles:
                return view(request, *args, **kwargs) 
            else:
                return HttpResponse('your are not authorise to  view this page')
        return wrapper_func
    return decorator


def admin_only(view):
    def wrapper_func(request, *args, **kwargs):
        group = None
        if request.user.groups.exists():
            group = request.user.groups.all()[0].name
        if group == 'customer':
            return redirect('user')
        else:
        	return view(request, *args, **kwargs)
      
    return wrapper_func

