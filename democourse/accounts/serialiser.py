from rest_framework import serializers, viewsets
from django.contrib.auth import get_user_model


# Serializers define the API representation.
user = get_user_model()
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = user
        fields = ['url', 'username', 'email']

# ViewSets define the view behavior.


class UserViewSet(viewsets.ModelViewSet):
    queryset = user.objects.all()
    serializer_class = UserSerializer
