from django.db.models import fields
from backend.democourse.accounts.models import Order
import django_filters
from .model import *


class OrderFilter(django_filters.FilterSet):
    """
    Description: Model Description
    """
    class Meta:
    	model = Order
    	fields = '__all__'
        